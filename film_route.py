from fastapi import APIRouter
from model.film_model import *

router = APIRouter()

def objIdToStr(obj):
    return str(obj["_id"])

@router.post("/dvdbyid")
async def view_search_books_id(params:dict):
    result = showDvdById(**params)
    return result

@router.get("/dvds")
async def view_search_dvd():
    result = showDvd()
    data_list=[]
    for dvd in result:
        dvd["_id"] = objIdToStr(dvd)
        data_list.append(dvd)
    return data_list