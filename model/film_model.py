from mongoengine import connect, Document, DynamicDocument
from mongoengine.fields import IntField, StringField, FloatField
from mongoengine import connect

connection = connect(db="tugas_akhir", host="localhost", port=27017)
if connection:
     print("MongoDB Connected")
    
class film(Document):
    filmid = StringField(required=True, max_length=20)
    title = StringField(required=True, max_length=70)
    years = IntField(required=True)
    country = StringField(required=True, max_length=20)
    genre = StringField(required=True, max_length=50)
    language = StringField(required=True, max_length=20)
    rating = FloatField(required=True)

#fungsi untuk melihat semua dvd
def showDvd():
    return print(film.objects.to_json())

#fungsi untuk melihat dvd berdasarkan film id
def showDvdById(**params):
    return film.objects(filmid = params['filmid']).to_json

#fungsi untuk melihat dvd berdasarkan genre
def showDvdByGenre(**params):
    return film.objects(genre = params['genre']).to_json

#fungsi untuk menambahkan dvd
def insertDvd(**params):
    film(**params).save()

#fungsi untuk mengubah data dvd
def updateDvdById(**params):
    result = film.objects(filmid = params['filmid'])
    result.update(**params)

#fungsi untuk menghapus dvd
def deleteDvdById(**params):
    result = film.objects(filmid=params['filmid'])
    result.delete()

dvdId = {
    "filmid" : "K1",
    "genre" : "Horror",
    "rating": 7.8
}

tambahDvd = {
    "filmid" : "W3",
    "title" : "Interstellar",
    "years" : 2014, 
    "country" : "US",
    "genre" : "Adventure", 
    "language" : "English",
    "rating" : 8.6
}

# showDvd()
# showDvdById(**dvdId)
# showDvdByGenre(**dvdId)
# insertDvd(**tambahDvd)
# updateDvdById(**dvdId)
# deleteDvdById(**tambahDvd)