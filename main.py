from fastapi import FastAPI
from film_route import router as film_router

app = FastAPI()
app.include_router(film_router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}
